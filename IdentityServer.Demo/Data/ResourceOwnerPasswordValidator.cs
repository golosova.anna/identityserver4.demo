﻿using IdentityServer4.Validation;
using System.Threading.Tasks;

namespace IdentityServer.Demo.Data
{
    public class ResourceOwnerPasswordValidator : IResourceOwnerPasswordValidator
    {
        public async Task ValidateAsync(ResourceOwnerPasswordValidationContext context)
        {
            // set the result
            context.Result = new GrantValidationResult(
                subject: "1",
                authenticationMethod: "custom");

            return;
        }
    }
}
