﻿using IdentityModel;
using IdentityServer4.Models;
using IdentityServer4.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace IdentityServer.Demo.Data
{
    public class JwtClaim
    {
        // Dzmitry Shauchuk
        // NB: You need to recompile all assemblies that use the const if it was changed.

        public const string UserId = "user_id";
        public const string Domain = "domain";
        public const string ChildDomain = "child_domain";
    }

    public class ProfileService : IProfileService
    {
        public async Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            var claims = new List<Claim>
            {
                new Claim(JwtClaim.UserId, "1"),
                new Claim(JwtClaimTypes.Name, "alice"),
                new Claim(JwtClaimTypes.GivenName, "alice"),
                new Claim(JwtClaimTypes.FamilyName, "aclice2"),
                new Claim(JwtClaimTypes.Email, "someEmail")
            };

            //set issued claims to return
            context.IssuedClaims = claims.Where(x => context.RequestedClaimTypes.Contains(x.Type)).ToList();

            return;
        }

        public async Task IsActiveAsync(IsActiveContext context)
        {
            context.IsActive = true;

            return;
        }
    }
}
