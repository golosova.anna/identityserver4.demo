﻿using IdentityServer4.Models;
using IdentityServer4.Stores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer.Demo.Data
{
    public class RefreshTokenStore : IRefreshTokenStore
    {
        private static Dictionary<string, RefreshToken> RefreshTokens = new Dictionary<string, RefreshToken>();

        public Task<RefreshToken> GetRefreshTokenAsync(string refreshTokenHandle)
        {
            return Task.FromResult(RefreshTokens.FirstOrDefault(r => r.Key.Equals(refreshTokenHandle)).Value);
        }

        public Task RemoveRefreshTokenAsync(string refreshTokenHandle)
        {
            return Task.FromResult(RefreshTokens.Remove(refreshTokenHandle));
        }

        public Task RemoveRefreshTokensAsync(string subjectId, string clientId)
        {
            var tokens = RefreshTokens.Where(r => r.Value.SubjectId.Equals(subjectId) && r.Value.ClientId.Equals(clientId));

            if (tokens != null && tokens.Any())
            {
                foreach (var t in tokens)
                {
                    RefreshTokens.Remove(t.Key);
                }
            }

            return Task.FromResult(0);
        }

        public Task<string> StoreRefreshTokenAsync(RefreshToken refreshToken)
        {
            var key = Guid.NewGuid().ToString();
            RefreshTokens.Add(key, refreshToken);

            return Task.FromResult(key);
        }

        public async Task UpdateRefreshTokenAsync(string handle, RefreshToken refreshToken)
        {
            var item = await GetRefreshTokenAsync(handle);

            if (item != null)
            {
                item.AccessToken = refreshToken.AccessToken;
                item.CreationTime = refreshToken.CreationTime;
                item.Lifetime = refreshToken.Lifetime;
                item.Version = refreshToken.Version;
            }
        }
    }
}
