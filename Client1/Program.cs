﻿using System;

namespace Client1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            // wait untill Api1 and IdentityServer.Demo project will start
            System.Threading.Thread.Sleep(10 * 1000);

            var tokenClient = new TokenClient1();

            var client2ValidToken = tokenClient.RequestPasswordTokenAsync("client.mobile2", "secret2", "api2").Result;
            IsNotNull(client2ValidToken);

            var client1ValidToken = tokenClient.RequestPasswordTokenAsync("client.mobile1", "secret1", "api1").Result;
            IsNotNull(client1ValidToken);

            var client2InValidToken = tokenClient.RequestPasswordTokenAsync("client.mobile2", "secret2", "api1").Result;
            IsNull(client2InValidToken);

            var client1InValidToken = tokenClient.RequestPasswordTokenAsync("client.mobile1", "secret1", "api2").Result;
            IsNull(client1InValidToken);


            var res = tokenClient.RevocationAsync(client1ValidToken.AccessToken, "client.mobile1", "secret1").Result;
            var resp = tokenClient.SendTestRequest(client1ValidToken.AccessToken, "api1").Result;
            IsNull(resp);

            //tockenClient.RequestClientCredentialsAsync("client.mobile", "secret").Wait();

            //tockenClient.RequestClientCredentialsAsync("client.mobile", "secret").Wait();
            //tockenClient.RequestDeviceAuthorizationAsync("client.mobile", "secret").Wait();


            //tockenClient.RequestClientCredentialsAsync("new.client", "secret.new").Wait();
            //tockenClient.RequestPasswordTokenAsync("new.client", "secret.new").Wait();

            Console.ReadKey();

            void PrintGreen(string message)
            {
                Console.ForegroundColor = ConsoleColor.Green;

                Console.WriteLine(message);

                Console.ForegroundColor = ConsoleColor.White;
            }

            void IsNull(object something)
            {
                if (something == null)
                    PrintGreen("Valid");
                else
                    PrintRed("Fail");
            }

            void IsNotNull(object something)
            {
                if (something != null)
                    PrintGreen("Valid");
                else
                    PrintRed("Fail");
            }
            void PrintRed(string message)
            {
                Console.ForegroundColor = ConsoleColor.Red;

                Console.WriteLine(message);

                Console.ForegroundColor = ConsoleColor.White;
            }
        }
    }
}
