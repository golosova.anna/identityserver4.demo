﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using IdentityModel;
using IdentityModel.Client;
using Newtonsoft.Json.Linq;

namespace Client1
{
    public class TokenClient1
    {
        private DiscoveryDocumentResponse _discoveryDocument
        {
            get => _client.GetDiscoveryDocumentAsync(
                new DiscoveryDocumentRequest
                {
                    Address = "http://localhost:5000",
                    Policy =
                        {
                            AuthorityValidationStrategy = new AuthorityUrlValidationStrategy()
                        }
                }).Result;
        }

        private HttpClient _client
        {
            get => new HttpClient();
        }

        public async Task<DeviceAuthorizationResponse> RequestAuthorizationAsync()
        {
            var response = await _client.RequestDeviceAuthorizationAsync(new DeviceAuthorizationRequest
            {
                Address = _discoveryDocument.DeviceAuthorizationEndpoint,
                ClientId = "client.mobile1",
                ClientSecret = "secret1"
            });

            if (response.IsError) throw new Exception(response.Error);

            Console.WriteLine($"user code   : {response.UserCode}");
            Console.WriteLine($"device code : {response.DeviceCode}");
            Console.WriteLine($"URL         : {response.VerificationUri}");
            Console.WriteLine($"Complete URL: {response.VerificationUriComplete}");

            Console.WriteLine($"\nPress enter to launch browser ({response.VerificationUri})");
            Console.ReadLine();

           // Process.Start(new ProcessStartInfo(response.VerificationUri) { UseShellExecute = true });
            return response;
        }

        public async Task RequestDeviceAuthorizationAsync(string clientId, string clientSecret, string scope)
        {
            var response = await _client.RequestDeviceAuthorizationAsync(new DeviceAuthorizationRequest
            {
                ClientId = clientId,
                ClientSecret = clientSecret,
                Scope = scope,
                Address = _discoveryDocument.DeviceAuthorizationEndpoint
            });

            if (response.IsError)
            {
                Console.WriteLine(response.Error);
                return;
            }

            Console.WriteLine(response.DeviceCode);
        }
        
        public async Task<TokenResponse> RequestClientCredentialsAsync(string clientId, string clientSecret, string scope)
        {
            Console.WriteLine($"Start getting token by [ClientCredentials] for a client #{clientId} with secret [{clientSecret}]");

            var tokenResponse = await _client.RequestClientCredentialsTokenAsync(new ClientCredentialsTokenRequest
            {
                Address = _discoveryDocument.TokenEndpoint,
                ClientId = clientId,
                ClientSecret = clientSecret,
                Scope = scope
            });

            if (tokenResponse.IsError)
            {
                Console.WriteLine(tokenResponse.Error);
                return null;
            }

            Console.WriteLine(tokenResponse.Json);

            await SendTestRequest(tokenResponse.AccessToken, scope);

            return tokenResponse;
        }

        public async Task<TokenResponse> RequestPasswordTokenAsync(string clientId, string clientSecret, string scope)
        {
            Console.WriteLine($"Start getting token by [PasswordToken] for a client #{clientId} with secret [{clientSecret}]");

            var tokenResponse = await _client.RequestPasswordTokenAsync(new PasswordTokenRequest
            {
                Address = _discoveryDocument.TokenEndpoint,
                GrantType = "password",
                ClientId = clientId,
                ClientSecret = clientSecret,
                Scope = scope,
                Password = "password",
                UserName = "alice"
            });

            if (tokenResponse.IsError)
            {
                Console.WriteLine(tokenResponse.Error);
                return null;
            }

            Console.WriteLine(tokenResponse.Json);

            await SendTestRequest(tokenResponse.AccessToken, scope);

            return tokenResponse;
        }


        public async Task<TokenRevocationResponse> RevocationAsync(string token, string clientId, string secret)
        {
            var tokenResponse = await _client.RevokeTokenAsync(new TokenRevocationRequest
            {
                Address = _discoveryDocument.RevocationEndpoint,
                Token = token,
                TokenTypeHint = "access_token",
                ClientId = clientId,
                ClientSecret = secret
            });

            if (tokenResponse.IsError)
            {
                Console.WriteLine(tokenResponse.Error);
                return null;
            }

            Console.WriteLine(tokenResponse.Json);

            return tokenResponse;
        }

        public async Task<string> SendTestRequest(string token, string scope)
        {
            var localClient = new HttpClient();

            localClient.SetBearerToken(token);

            HttpResponseMessage response = null;

            if (scope == "api1")
                response = await localClient.GetAsync("http://localhost:5001/WeatherForecast");

            if (scope == "api2")
                response = await localClient.GetAsync("http://localhost:5002/WeatherForecast");

            if (!response.IsSuccessStatusCode)
            {
                var content = await response.Content.ReadAsStringAsync();
                Console.WriteLine(content);
                Console.WriteLine(response.StatusCode);
                return null;
            }
            else
            {
                var content = await response.Content.ReadAsStringAsync();
                Console.WriteLine(JArray.Parse(content));
                return content;
            }
        }
    }
}
